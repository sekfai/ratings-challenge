$(document).ready(function() {
  callGetProductApi();
  setInterval(refresh, 5000);
  $('form .rating').rate({
    max_value: 5,
    step_size: 0.5,
    initial_value: 4,
  });
  $('input[type="submit"]').on("click", function (event) {
    event.preventDefault();
    callAddReviewApi();
  });
});

function callAddReviewApi() {
  var rating = $('form .rating').rate('getValue');
  var description = $('#description').val();
  var apiUrl = 'http://localhost:8080/add-review/1?rating=' + rating + '&description=' + description;
  $.ajax({
    type: "POST",
    url: apiUrl,
    async: false,
    success: function (response) {
      //var json = JSON.stringify(response);
      //alert(json);
      //clear();
      //populateProductInfo(response);
      appendReview(response);
      $('form').trigger('reset');
      $('form .rating').rate("setValue", 4); //reset to 4
    },
    error:function (xhr, ajaxOptions, thrownError){
        if(xhr.status==404) {
            alert(thrownError);
        }
    }
  });
}

function appendReview(data) {
  var oldReviewsCount = $('#review_list tr').length;
	console.log(oldReviewsCount)
  var averageRating = (calcTotalRating()  + data.rating) / (oldReviewsCount + 1);
	console.log(averageRating)
  var rounded = Math.round(averageRating*10) / 10;
	console.log(rounded)
  $('#average_rating td:first').text(rounded);
  if (oldReviewsCount === 0) {
    $('#average_rating .rating').remove();
    td = "<td class='rating'></td>";
    $('#average_rating tr').append(td);
    $("#average_rating .rating").rate({
      max_value: 5,
      step_size: 0.5,
      initial_value: rounded,
      readonly: true,
    });
  } else {
    $('#average_rating .rating').rate("setValue", rounded);
  }

  var row = "<tr style='display: none;'>"
  + "<td class='rating'></td>"
  + "<td class='text'>" + data.rating + ", " + data.description + "</td>"
  + "</tr>";
  $('#review_list tbody').append(row);
  $("#review_list tr:last .rating").rate({
    max_value: 5,
    step_size: 0.5,
    initial_value: data.rating,
    readonly: true,
  });
  $("#review_list tr:last").fadeIn('slow');
}

function refresh() {
    clear();
    callGetProductApi();
}

function clear() {
  $('#average_rating tbody').empty();
  $('#review_list tbody').empty();
}

function callGetProductApi() {
    $.ajax({
      type: "GET",
      url: "http://localhost:8080/get-product/1",
      async: false,
      success: function (response) {
	//var json = JSON.stringify(response);
	//alert(json);
	populateProductInfo(response);
      },
      error:function (xhr, ajaxOptions, thrownError){
          if(xhr.status==404) {
              alert(thrownError);
          }
      }
    });
}

function calcTotalRating(){
  var sum = 0;
  $("#review_list .text").each(function(){
    sum += parseFloat($(this).text());
  });
  return sum;
}

function populateProductInfo(data) {
  var product = data.product;
  $('#title').text(product.name);
  $.each(product.reviews, function (i, review) {
    var row = "<tr>"
    + "<td class='rating'></td>"
    + "<td class='text'>" + review.rating + ", " + review.description + "</td>"
    + "</tr>";
    $('#review_list tbody').append(row);
    $("#review_list tr:last .rating").rate({
      max_value: 5,
      step_size: 0.5,
      initial_value: review.rating,
      readonly: true,
    });
  });

  var averageRating = product.reviews.reduce((total, next) => total + next.rating, 0) / product.reviews.length;
  averageRating = averageRating || 0;
  var rounded = Math.round(averageRating*10) / 10;
    var row = "<tr>"
    + "<td>" + rounded + "</td>"
    + "<td class='rating'></td>"
    + "</tr>";
    $('#average_rating tbody').append(row);
    $("#average_rating .rating").rate({
      max_value: 5,
      step_size: 0.5,
      initial_value: rounded,
      readonly: true,
    });
}
