from flask import Flask, render_template, flash, request, abort
import os
import json
import time


# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['PRODUCTS'] = app.root_path + '/static/data'

@app.route('/')
def index():
  print(app.root_path)
  print(app.config['PRODUCTS'])
  return render_template('index.html')

@app.route('/get-product/<id>')
def get_product(id):
  try:
    with open(os.path.join(app.config['PRODUCTS'], 'product'+id+'.json')) as f:
      data = json.load(f)
    return data
  except FileNotFoundError:
    abort(404)

@app.route('/add-review/<id>', methods=['POST'])
def add_review(id):
  rating = float(request.args.get('rating'))
  description = request.args.get('description')
  review = {'rating': rating, 'description': description}
  try:
    with open(os.path.join(app.config['PRODUCTS'], 'product'+id+'.json')) as f:
      data = json.load(f)
    data['product']['reviews'].append(review)
    with open(os.path.join(app.config['PRODUCTS'], 'product'+id+'.json'), 'w') as f:
      json.dump(data, f)
    return review
  except FileNotFoundError:
    abort(404)

if __name__ == '__main__':
    app.run(port=8080, debug=True)
